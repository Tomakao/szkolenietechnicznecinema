﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SzkolenieTechniczne.Cinema.Storage.Common
{
    [Table("MovieCategories", Schema="Cinema")]
    public class MovieCategory : BaseEntity
    {
        private MovieCategory() { }
        public MovieCategory(string category)
        {
            Category = category;
        }

        [Required]
        [MinLength(1)]
        [MaxLength(20)]
        public string Category { get; set; }
    }
}
