﻿using SzkolenieTechniczne.Cinema.Storage.Common;


namespace SzkolenieTechniczne.Cinema.Storage.Repository
{
    public interface IMovieRepository
    {
        List<Movie> GetMovies();
        Movie GetMovieById(long movieId);

        void AddMovie(Movie movie);
        void EditMovie(Movie movie);
        void RemoveMovie(long id);
        bool IsMovieExist(long movieId);
        bool IsMovieExist(string name, int year);
        bool IsSeanceExist(DateTime date);

        void AddSeance(Seance seance);
        void BuyTicket(Ticket ticket);
        List<Seance> GetSeancesByMovieId(long movieId);
        Movie GetSeanceDetails(long movieId);
    }
}
