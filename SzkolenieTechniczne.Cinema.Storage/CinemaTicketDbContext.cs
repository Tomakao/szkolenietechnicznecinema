﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SzkolenieTechniczne.Cinema.Storage.Common;

namespace SzkolenieTechniczne.Cinema.Storage.Repository
{
    public class CinemaTicketDbContext : DbContext
    {
        private IConfiguration _configuration { get; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Seance> Seances { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<MovieCategory> MovieCategories { get; set; }

        public CinemaTicketDbContext(IConfiguration configuration)
            : base()
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
           options.UseSqlServer(@"server = 10.200.2.28; Database = cinema-dev-w69047; User Id = stud; Password = wsiz;",
           //options.UseSqlServer(@"server=(localdb)MSSQLLocalDB;Database = Cinema - dev1;tristed_connection = true;",
                // options.UseSqlServer("",
                x => x.MigrationsHistoryTable("__EFMigrationsHistory", "Cinema"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
