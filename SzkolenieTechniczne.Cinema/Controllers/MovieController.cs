﻿using Microsoft.AspNetCore.Mvc;
using SzkolenieTechniczne.Cinema.Service.Command.Movie.Add;
using SzkolenieTechniczne.Cinema.Service.Query.Dtos;
using SzkolenieTechniczne.Cinema.Storage.Repository;

namespace SzkolenieTechniczne.Cinema.Controllers
{
    public class MovieController : Controller
    {
        private static List<MovieDto> _movies = new List<MovieDto>()
        {
            new MovieDto("Terminator", 1),
            new MovieDto("Rambo 1", 2)
        };

        public MovieController()
        {

        }
        public IActionResult Index()
        {
            return View(_movies);
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(AddMovieCommand command)
        {
            _movies.Add(new MovieDto(command.Name, 1));

            return RedirectToAction("Index");
        }

        /*public IActionResult Edit(long id)
        {

        }*/

        [HttpPost]
        public IActionResult Delete(long id)
        {
            return RedirectToAction("Index");
        }
    }
}
