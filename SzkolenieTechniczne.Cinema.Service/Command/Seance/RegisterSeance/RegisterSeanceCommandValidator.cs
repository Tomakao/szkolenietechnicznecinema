﻿using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace SzkolenieTechniczne.Cinema.Service.Command.Seance.RegisterSeance
{
    internal class RegisterSeanceCommandValidator : AbstractValidator<RegisterSeanceCommand>
    {
        public RegisterSeanceCommandValidator() { 
            RuleFor(x => x.MovieId).NotEmpty();
            RuleFor(x => x.NumberOfTickets).NotEmpty().GreaterThan(1);
            RuleFor(x => x.SeanceDate).NotEmpty().GreaterThan(DateTime.UtcNow);
        }
    }
}
